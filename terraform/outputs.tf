output "alb_dns_name" {
    value = aws_lb.tf-example-lb.dns_name
    description = "The dns name of the lb"
}

