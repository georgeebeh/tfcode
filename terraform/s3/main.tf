provider "aws" {
    region = "eu-west-2"
}

output "s3_bucket_arn"{
    value = aws_s3_bucket.dev-tf-state.arn
    description = "The arn of the s3 bucket"
}

output "dynamodb_table_name" {
    value = aws_dynamodb_table.dev-tf-locks.name
    description = "The name of the dynamodb table"
}

resource "aws_s3_bucket" "dev-tf-state" {
    bucket = "ebeh-dev-tf-state"

  # Prevent accidental delection of this s3 bucket
    lifecycle {
        prevent_destroy = true
    }
}

resource "aws_s3_bucket_versioning" "enabled" {
    bucket = aws_s3_bucket.dev-tf-state.bucket
    versioning_configuration {
        status = "Enabled"
    }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "default" {
    bucket = aws_s3_bucket.dev-tf-state.bucket
    rule {
        apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"
        }
    }
}

resource "aws_s3_bucket_public_access_block" "public_access" {
    bucket = aws_s3_bucket.dev-tf-state.bucket
    block_public_acls = true
    block_public_policy = true
    ignore_public_acls = true
    restrict_public_buckets = true
}

resource "aws_dynamodb_table" "dev-tf-locks" {
    name = "dev-tf-locks"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"
    attribute {
        name = "LockID"
        type = "S"
    }
}

